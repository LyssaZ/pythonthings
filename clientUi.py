#!/bin/python
import socket
import threading
import sys
from tkinter import *
import time

global passSet 
passSet = False
port = 9999
global run 
run = True
global s
global us
global username
us = True
window = Tk()
window.geometry('400x500')
window.resizable(False, False)



def isInt(Test):
	try:
		int(Test)
		return True
	except ValueError:
		return False


def SendText():
	#Globals s = socket, us = usernameSet	
	global us
	global s
	global username
	global passSet
	
#Username setting func
	if us:
		if getText.get() != "":
			if not passSet:
				username = getText.get()
			
				s.send(username.encode())
				
			else:
				
				password = getText.get()
				s.send(password.encode())

	
			getText.delete(0,END)
		else:
			chatbox.insert(END,("Invalid input\n"))
	if getText.get() != "":
		if getText.get().startswith('!'):
			funcPar(username)
		
		else:
	
			
			s.send(getText.get().encode())
			
			chatbox.insert(END, ("You: "+getText.get()+ "\n"))
			getText.delete(0,END)

#Parses user commands
def funcPar(username):
	if "info" in getText.get():
		chatbox.insert(END,("Username: " + username+ "\nHostname: "+ socket.gethostname() + "\nOS: " + sys.platform+"\n"+"IP: ")+socket.gethostbyname(socket.gethostname())+"\n")
		
	elif "clear" in getText.get():
		chatbox.delete(1.0,END)
	elif "help" in getText.get():
		chatbox.insert(END,("Commands:\n info: Print user info\n clear: Clears chat area\n !spam: Spams up to 5 messeges\n "))
	elif "spam" in getText.get():
		e = getText.get().split("spam")
		if len(e) > 1:
			if isInt(e[1]):
			
				if int(e[1]) < 6:
					for x in range(int(e[1])):
						time.sleep(0.1)
						s.send("Spam".encode())
	elif "set" in getText.get():
		if "header" in getText.get():
			t = getText.get().split("=")
			print(t)
			if len(t) > 1:
				print(t[1])
				try:
					w.configure(bg= t[1].replace(" ", ""))
				except:
					print("Invalid color")
		elif "size" in getText.get():
			t = getText.get().split("=")
			if len(t) > 1:
				print(t[1])
				if isInt(t[1]):
					chatbox.configure(font=("Open Sans", t[1]))
					#window.update()			
	getText.delete(0,END)


def En(fff):
	SendText()


#Colors
#bgM = "#1c2343"
bgS = "#e6e8ed"
bgM = "White"
fgM = "d"
fgS = "black"
bgD = "#495A72"

#Borders side/top
#Right border
RightBorder  = Frame(window, height = 8, width = 8, bg = bgM,highlightthickness = 0, borderwidth = 0 )
#Left border
LeftBorder = Frame(window, height = 8, width = 8, bg = bgM,highlightthickness = 0, borderwidth = 0 )
#Top border
TopBorderTop = Frame(window,height = 8, width = 4, bg = bgM,highlightthickness = 0, borderwidth = 0)
#Bottom top border
BottomBorderTop = Frame(window,height = 8, width = 4, bg = bgM,highlightthickness = 0, borderwidth = 0)

#Prevents chatbox from resizing when text size is changed
ChatFrame = Frame(window, height = 420, bg = bgS,highlightthickness = 0, borderwidth = 0, )
ChatFrame.pack_propagate(False)


#Logo/header
w = Label(window, text="IntraChat | Test Build 0.3", bg = "#938581", fg = fgS, font=("Open Sans", 10, "bold"))




RightBorder.pack(side = RIGHT)
RightBorder.pack(fill = Y)


LeftBorder.pack(side = LEFT)
LeftBorder.pack(fill = Y)
BottomBorderTop.pack(side = TOP)
BottomBorderTop.pack(fill = X)
w.pack(fill = X)
BottomBorderTop.pack(fill = X)
ChatFrame.pack(fill = X)



chatbox = Text(ChatFrame, height = 25)
chatbox.insert(END,"Enter your username:\n")
chatbox.configure(padx = 20,pady = 20,background= bgS,fg = fgS ,highlightthickness = 0, highlightbackground= bgM,highlightcolor= bgM, borderwidth = 0,font=("Open Sans", 9), height = 30  )
chatbox.pack(fill = X)

#GET TEXT
getText = Entry(window,bg = bgS,fg = fgS, highlightbackground= bgS, highlightcolor= bgS,highlightthickness = 0, borderwidth = 0)
#GET TEXT
#getText.configure(textvariable=password, show='*')

#Button
button = Button(window, text='Send', width=10,bg = bgS,fg = fgS, command=SendText, highlightbackground= bgS,highlightcolor= bgS,highlightthickness = 0, borderwidth = 0 )
#Button


pad = Frame(window, height = 30, width = 20, bg = bgM,highlightthickness = 0, borderwidth = 0 )
pad2 = Frame(window, height = 30, width = 20, bg = bgM,highlightthickness = 0, borderwidth = 0 )
pad5 = Frame(window, height = 8, width = 4, bg = bgM,highlightthickness = 0, borderwidth = 0 )
window.configure(bg = bgM)
pad6 = Frame(window, height = 8, width = 4, bg = bgM,highlightthickness = 0, borderwidth = 0 )



pad6.pack(fill = X)
pad5.pack(side = BOTTOM)
pad5.pack(fill = X)
getText.pack(side = LEFT)
getText.pack(ipady=3)
button.pack(side = RIGHT)


window.bind('<Return>', En)




#Main logic 

#Test class
class ok():
	run = True
	def getRun():
		return ok.run
	def setRun(state):
		ok.run = state



ip =  socket.gethostbyname(socket.gethostname())
print(ip)
#ipR = "172.20.4.96"
#ip2 = "172.20.201.145"
#ipN = "192.168.1.157"
if run:
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((ip, port))


	
def listen():
	global s
	global run
	global us
	global passSet	
	while ok.getRun():
		try:
			e = s.recv(1024)
			print("data recived")
			if us and e.decode() == "!U":
				
				us = False
			elif us and e.decode() == "!pass":
				getText.configure(show='*')
				passSet = True
				#chatbox.insert(END,"Username taken\n")
			else:
				passSet = False
				getText.configure(show="")
				chatbox.insert(END, (e.decode()+"\n"))
		except:
			ok.setRun(False)
			print("fail")


f = threading.Thread(target = listen).start()
window.mainloop()

ok.setRun(False)
s.close()
del s
#f.close()
#f.stop()
sys.exit()	
