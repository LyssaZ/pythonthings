#!/bin/python
import socket
import sys
import threading
import time
import sys
import mysql.connector
import bcrypt
from tkinter import *
#Needs to imported seprativly from tkinter *
from tkinter import messagebox
from tkinter import simpledialog


#Database settings
DBH = "localhost"
DUser = "root"
DPassword = "GoodPassword"




#Theme class. Change the values to change the colors of the ui. Defualt is the CharUi theme
class theme():
	bgS = "#e6e8ed"
	bgM = "White"
	#fgM = "d"
	fgS = "black"
	bgD = "#495A72"

#Db manger ui
class UI():
	def __init__(self, master, mq):
		#Main ui stuff
		self.master = master
		master.config(bg = theme.bgM)
		master.title("Db Manger IntraChat v0.1")
		self.UserC = Listbox(self.master,width = 40,bg = theme.bgS, highlightthickness = 2, borderwidth = 0, fg = theme.fgS)	
		self.CreateU = Frame(self.master,width = 20,bg = theme.bgM, highlightthickness = 0, borderwidth = 0)	
	


		#Main buttons and labels	
		self.TitleBar = Frame(self.master,bg = theme.bgM, borderwidth = 0, highlightthickness  = 0)
		self.TitleBar.pack(side= TOP)
		self.TitleBar.pack(fill = X)
		self.BFrame = Frame(self.master, bg = theme.bgM, borderwidth = 0, highlightthickness  = 0, height = 50, padx= 10, pady = 8)
		#self.BFrame.pack(side = RIGHT)
		self.BFrame.pack(side = BOTTOM)
		self.BFrame.pack(fill = X)	
		self.removeUser = Button(self.BFrame, text='Remove User', command= lambda: self.DeleteUser(mq), bg = theme.bgS, fg = theme.fgS, width = 10, borderwidth = 0, highlightthickness  = 0)
		self.changePass = Button(self.BFrame, text='Change Pass', command= lambda: self.ChangePass(mq), bg = theme.bgS, fg = theme.fgS, width = 10, borderwidth = 0, highlightthickness  = 0)
		self.removeUser.pack(side = LEFT)
		self.changePass.pack(side = LEFT)

		self.CreateU1 = Frame(self.CreateU,width = 20,bg = theme.bgM, highlightthickness = 0, borderwidth = 0)
		self.CreateU2 = Frame(self.CreateU,width = 20,bg = theme.bgM, highlightthickness = 0, borderwidth = 0)
		self.CreateU.pack(side = RIGHT)
		self.CreateU.pack(fill = Y)

		self.UserName = Entry(self.CreateU2, width = 20, bg = theme.bgS, fg = theme.fgS)
		self.PassWord = Entry(self.CreateU1, width = 20, bg = theme.bgS, show="*", fg = theme.fgS)
	
		#User create entry		
		self.CreateU2.pack(side= TOP)				
		self.CreateU1.pack(side = TOP)
		self.UserNameL = Label(self.CreateU2, text="Username:", bg = theme.bgM, fg = theme.fgS, width = 10)	
		self.PassWordL = Label(self.CreateU1, text="Password:", bg = theme.bgM, fg = theme.fgS, width = 10)
		self.UserNameL.pack(side = TOP)   	
		self.UserNameL.pack(side = LEFT) 	
		self.UserName.pack(side = TOP) 	
		self.UserName.pack(side = RIGHT)
	
		

		#Places the menus	
		self.PassWordL.pack(side = BOTTOM)
		self.PassWordL.pack(side = LEFT)
		self.PassWord.pack(side = BOTTOM)
		self.PassWord.pack(side = LEFT)
		self.cB = Button(self.CreateU, text='Create', command= lambda: self.CreateUser(mq), bg = theme.bgS, fg = theme.fgS, width = 10, borderwidth = 0, highlightthickness  = 0)
		self.cB.pack(side= TOP)
		self.Info = Label(self.TitleBar, text="User List", bg = theme.bgM, fg = theme.fgS)
		self.title = Label(self.TitleBar, text="IntraCHAT DB manger", bg = theme.bgM, fg = theme.fgS)
		self.title.pack(side= TOP)
		

		self.Info.pack(side= BOTTOM)

		#self.label.pack(fill = Y)
		#self.label.pack(side = LEFT)
		
		self.exit = Button(self.BFrame, text="Exit", command=master.quit,bg = theme.bgS, fg = theme.fgS, width = 10  , borderwidth = 0, highlightthickness  = 0)
		self.exit.pack(side = RIGHT)
		

		self.UserC.pack( side = LEFT)
		self.UserC.pack(fill = Y)
		self.getList(mq)
	#Parses the errors and calls the create user func		
	def CreateUser(self, mq):
		UserName = self.UserName.get()
		Password = self.PassWord.get()
		res = CreateUser(mq,UserName, CryptPassword(Password,"Master Key"))
		if res == 1:
			messagebox.showinfo("UserName taken","Username Taken")
		elif res == 0:
			messagebox.showinfo("Bad username","Username is invalid. No spaces or names longer than 10 char")
		else:
			messagebox.showinfo("User Created", "User " + UserName+" was created succefully")
			self.UserC.delete(0,'end')
			self.getList(mq)
		self.UserName.delete(0,END)
		self.PassWord.delete(0,END)
	#Updates the password in the database
	def ChangePass(self,mq):
		
		
		UserName = self.UserC.get(ACTIVE)
		if len(UserName) > 0:
		#li = map(int, self.UserC.curselection())
			Password = simpledialog.askstring("Password", "Enter the new password",parent=self.master)
			
			if Password != None:
				ChangePassword(mq, UserName[0], CryptPassword(Password,"Master Key"))
				messagebox.showinfo("Succes","The Password for user " +UserName[0]+ " was changed succefully")
		else:
			messagebox.showinfo("No user Selected","No selecstion was made")

	#Gets the lists of users from the db

	def getList(self,mq):
		UserList = getallUsers(mq)
		for x in UserList:
			self.UserC.insert(END, x)	
		
	def DeleteUser(self,mq):	
		User = self.UserC.get(ACTIVE)
		
		if len(User) > 0:
			print(User[0]) 	
			res = DeleteUser(mq,User[0])
			if res == 1:
				messagebox.showinfo("User Not found","Faild to delete user. user not found in db")
			else:
				messagebox.showinfo("Succes!", "User " + User[0] + "Was Succefully deleted!")
		else:
			messagebox.showinfo("No user Selected","No selecstion was made")

		self.UserC.delete(0,'end')
		self.getList(mq)
#Fetched all user info from the db
def getallUsers(Dh):
	Dh.execute("SELECT username FROM users")
	return Dh.fetchall()


#Extract salt key from passwords stored in the db
def getSalt(st):
	return st[0:29]

#fetched the password stored in the db
def getPassword(Dh,username):
	
	Dh.execute("SELECT * FROM users WHERE username='"+username+"'")
	res = Dh.fetchall()
	if len(res) > 0:
		Dh.execute("SELECT password  FROM users WHERE username='"+username+"'")
		res = Dh.fetchone()
		return res[0]
	else:
		return False
#Compares the passwords. returns True if password matched
def checkPassword(Word,raw, master_secret_key):
	s = getSalt(Word)
	s = s.encode()
	combo_password = raw + str(s) + master_secret_key
	hashed_password = bcrypt.hashpw(combo_password.encode('utf-8'), s)
	#print(hashed_password)
	#print(Word)
	#print(raw)
	if Word == hashed_password.decode():
		return True
	else:
		return False
#Encrpys the password. Generates a random saltkey and stores it with the password
def CryptPassword(raw_password, master_secret_key):
	
	salt = bcrypt.gensalt()
	#print(salt)
	combo_password = raw_password + str(salt) + master_secret_key
	hashed_password = bcrypt.hashpw(combo_password.encode('utf-8'), salt)
	return hashed_password.decode()


#Creates a new user. 
#0 bad username, 1 username taken
def CreateUser(Dh, username,password):
	if len(username) >= 10 or " " in username or len(username) < 1:
		return 0

	Dh.execute("SELECT * FROM users WHERE username='"+username+"'")
	res = Dh.fetchall()
	if len(res) > 0:
		return 1
	#print("succes")	
	Dh.execute("INSERT INTO users(username,password) VALUES(\""+username+"\",\""+password +"\")")
	


#Removes a user from the db
#1 = User not found
def DeleteUser(Dh,username):
	#Checks if the username exists before trying to remove it
	Dh.execute("SELECT * FROM users WHERE username='"+username+"'")
	res = Dh.fetchall()
	if len(res) < 1:
		return 1
	Dh.execute("DELETE FROM users WHERE username=\""+username+"\"")


#Changes the password of the user	
def ChangePassword(Dh,username,password):
	Dh.execute("SELECT * FROM users WHERE username='"+username+"'")
	res = Dh.fetchall()
	if len(res) < 1:
		return 1
	Dh.execute("UPDATE users SET password =\""+password+"\" WHERE username =\""+username+"\"")


#cheks if the user exists. used to validated login
def UserExists(Dh,username):
	
	Dh.execute("SELECT * FROM users WHERE username='"+username+"'")
	res = Dh.fetchall()
	if len(res) > 0:
		return True
	else:
		return False


#Prevents the server from starting if -DBM or -setup was used
start = True





#Db conn settings
mydb = mysql.connector.connect(
  host=DBH,
  user=DUser,
  passwd=DPassword,
  autocommit=True
 
)
#Creates db handler
mq = mydb.cursor()
#Checks for startup arguments
if len(sys.argv) > 1:
	#Creates the db if -setup was used
	if sys.argv[1] == "-setup":
		mq.execute("CREATE DATABASE chat")
		mq.execute("USE chat")
		mq.execute("CREATE TABLE users (id INT AUTO_INCREMENT PRIMARY KEY, username VARCHAR(25), password VARCHAR(150))")
		start = False	
	#Starts the Datamangemnt ui if -dbm was used
	elif sys.argv[1] == "-dbm":
			mq.execute("USE chat")
			print("Staring Database mangement UI") 
			root = Tk()

			root.geometry('600x500')
			root.resizable(False, False)
			my_gui = UI(root, mq)
			start = False
			root.mainloop()



#Sets the db t
mq.execute("USE chat")
#Prints starts up info for the server
if start:
	port = 9999
	ip = socket.gethostbyname(socket.gethostname())
	print("server is running on ip:",ip, "and the port",port)
else:
	print("Goodbye!")
#Old ips
#ipw = "172.20.201.145"
#ipN = "192.168.1.157"
#ipR = "172.20.4.96"

#s = server(ip,port)
class ThreadedServer(object):
	def __init__(self, host, port):
		self.host = host
		self.port = port
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.sock.bind((self.host, self.port))
		self.clients = []
		self.cUserNames = []
		self.ComboList = {}
	
	
#Creates a thread for every connected user

	def listen(self):
		
		self.sock.listen(5)
		while True:
			client, address = self.sock.accept()
			self.clients.append(client)
			#client.settimeout(60)
			 
			
			threading.Thread(target = self.listenToClient,args = (address,client)).start()
		
	def listenToClient(self,address,client):
		size = 1024
		dL = 0
		Loop = True
		Notlogged = True
		client.send("Welcome to the 2BF server".encode())
		while Notlogged:
			print("Loop pass")
			UserName = client.recv(1024).decode()
			if UserName not in self.cUserNames and UserExists(mq, UserName) :
				client.send(("Enter the Password for the user " + UserName).encode())
				#Fixes a rare bug where !pass would be combined with the line above
				time.sleep(0.1)
				client.send("!pass".encode())
				password = client.recv(1024).decode()
				print(password)
				#Checks if the password given is correct
				time.sleep(0.1)	
				if checkPassword(getPassword(mq,UserName),password,"Master Key"):
							
					print("user password matched")
					self.cUserNames.append(UserName)
					self.ComboList[UserName] = client
					#Debug text
					print("send data")
					#Send the logged in flag to the client
					client.send("!U".encode())
					Notlogged = False
					client.send(("Logged in as " + UserName).encode())
			else:
				client.send("Invalid user name/invalid password".encode())
				#client.send("!Taken".encode())	
		
	
		self.sendToAll(client,(UserName + " has loged in").encode())
		while Loop:
			try:
				data = client.recv(size)
				#print(data.decode())
				
				
				if data:
					#print("pass")
					#self.client[i].send(data)
					data = data.decode()
					if data.startswith("/"):
						print("Command rutin called ")
						#self.serverCM(client,data)
						

						if data.startswith("/pm"):
							r = data.split('/pm',1)[1]
							print(r)
							print("Before Allways true")	
							if True:
								#print("WORK")	
								
								#print(s)
								
							
								r = r.split(" ",2)
								#r = r.split(' ',1)[1]
								#print("R full string: ",r)
								#print("r index 0: ", r[1])
								#print("r index 1: ",r[2])
								#print("r index 2: ", r[2])
								for x in range(len(self.cUserNames)):
									#print(x)
									if self.cUserNames[x] == r[1]:
										print("Match found")
										try:
											print("Trying dm")
											print(r[1])
											self.ComboList[r[1]].send(("Pm::"+UserName+": "+   r[2]).encode())
											print("dm sent")
										except: 
											print("Dm faild")
											
										break
						elif data.startswith("/users"):
								print("Clients func called")
								tmp = ""
								for i in self.cUserNames:
									tmp+= "\n" + i 
								print(tmp)
								try:
									client.send(("Users online:"+ tmp).encode())
								except:
									print("Client is dead")

						
						#stopSpam = True	
						print("End")
					else:
						data = (UserName+": "+ data).encode()
						self.sendToAll(client,data)
					dL = 0
				else:
					dL += 1
					print("no data ")
					if dL > 3:
						self.sendToAll(client,(UserName +" has loged off").encode())
						self.CleanUp(client,UserName) 		
						Loop = False
						break
				del data	
			except:	
				try:
					
					self.sendToAll(client,(UserName +" has loged off").encode()) 				
				except:
					client.close()
					print("mega dead")
					return False
				print("Fail")
				
		self.CleanUp(client,UserName)
		return False
	#Sends a messeges to all connected clients
	def sendToAll(self,client, msg):
			
		for x in range(len(self.clients)):
			#print("Trying to send§")	
			try:
		
				if self.clients[x] != client:
					print("trying to send data to",x)
					self.clients[x].send(msg)
			except:
				if x:
					print("client", x ,"is dead")
					try:
						
						self.clients.remove(self.clients[x])
					except:
						print("bad")
		
	#Tries to clen up after a user leaves the server	
	def CleanUp(self,client,UserName):
		try:
			client.send("".encode())
		except:
			print("send kill pid")
		self.clients.remove(client)
		self.cUserNames.remove(UserName)
		client.close()


def test():
	print("test")

	
if __name__ == "__main__" and start:
	threading.Thread(target = ThreadedServer(ip,port).listen()).start()
#if input("Command: ") == "Exit":
	#EXIT()	



