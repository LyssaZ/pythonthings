#!/bin/python


class bcolors:
	HEADER = '\033[95m'
	OKBLUE = '\033[94m'
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'



def testprint():
	print(bcolors.HEADER + "This line has TermColors.bcolors.HEADER" + bcolors.ENDC)
	print(bcolors.OKBLUE + "This line has TermColors.bcolors.OKBLUE" + bcolors.ENDC)
	print(bcolors.OKGREEN +"This line has TermColors.bcolors.OKGREEN" + bcolors.ENDC)
	print(bcolors.WARNING+ "This line has TermColors.bcolors.WARNING" + bcolors.ENDC)
	print(bcolors.FAIL + "This line has TermColors.bcolors.FAIL" + bcolors.ENDC)
	print(bcolors.ENDC + "This line has TermColors.bcolors.ENDC" + bcolors.ENDC)
	print(bcolors.BOLD+ "This line has TermColors.bcolors.BOLD" + bcolors.ENDC)
	print(bcolors.UNDERLINE +"This line has TermColors.bcolors.UNDERLINE" + bcolors.ENDC)

#testprint()
