import random
global i
global warning
i = 0
warning = False 


def isInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False




def getRAN(start, end):
	global i
	global warning
	i+=1
	if(end > start):
		return random.randrange(start,end)
	else:
		if warning == False:
			print("End must be bigger then Start, defaulting to 0-100 ")
			warning = True
		return random.randrange(0,100)
