#!/bin/python2.7

import turtle
import os
#sets up the screen 
wn = turtle.Screen()
wn.bgcolor("black")
wn.title("Space Invaders")

#penup = hides traces 


#game logo

logo = turtle.Turtle()
logo.color("white")
logo.penup()
logo.setposition(0,310)
logo.hideturtle()
logo.write("Space Invader", move=False, align='center', font=('Open Sans', 12, 'normal'))



#Logo drawing function
def drawlogo(text):

	logo.write(text, move=False, align='center', font=('Open Sans', 12, 'normal'))



#Draw text 
def drawborder(color):

	border_pen = turtle.Turtle()
	border_pen.hideturtle()
	border_pen.speed(10)
	border_pen.color(color)
	border_pen.penup()
	border_pen.setposition(-300,-300) 
	border_pen.pensize(3)
	border_pen.pendown()

	for side in range(4):
		border_pen.fd(600)
		border_pen.lt(90)

'''
Test
print("lol")

'''




#player
 
player = turtle.Turtle()
player.color("blue")
player.shape("triangle")
player.penup()
player.speed(0)
player.setposition(0,-250)
player.setheading(90)
#player probs
playerspeed = 20


#Player weapon
bullet = turtle.Turtle()
bullet.color("red")
bullet.shape("triangle")
bullet.penup()
bullet.speed(0)
bullet.setheading(90)
bullet.shapesize()
bullet.hideturtle()

bulletspeed = 30
#Define bullet state
bulletstate = True

#Game varibales
gameloop = True
score = 0

def endgame():
	global gameloop
	gameloop = False

def addscore(ascore, score):
 
	score+=ascore
	return score

	#move the player left
def move_left():
        x = player.xcor()-playerspeed
	if x < -280:
		x = -280
	player.setx(x)


#move right
def move_right():
	x = player.xcor()+playerspeed
	if x > 280:
		x = 280
	player.setx(x)


def fire_bullet(): 
	#Declare bullet state as global
        global bulletstate 
        
	#move the bullet 
        if bulletstate:
		x = player.xcor()
		y = player.ycor() + 20
		bullet.setposition(x, y)
		bullet.showturtle()
		bulletstate = False



#enemy 



enemy = turtle.Turtle()
enemy.color("red")
enemy.shape("circle")
enemy.penup()
enemy.speed(0)
enemy.setposition(-200, 250)
enemyspeed = 2


class enemyC:
	enCmy = turtle.Turtle()
	enemy.color("red")
	enemy.shape("circle")
	enemy.penup()
	enemy.speed(0)
	enemy.setposition(-200, 250) 	
	enemyspeed = 2



#create keybaord 
turtle.listen()
turtle.onkey(move_left,"Left")
turtle.onkey(move_right, "Right")
turtle.onkey(fire_bullet, "space")
turtle.onkey(endgame, "q")
#Main game loop



drawborder("white")
score




while gameloop:
	enemy.setx(enemy.xcor()- enemyspeed)
	if player.xcor() == 280 or player.xcor() == -280:
		player.color("red")
		#wn.bgcolor("green")
		#drawborder("White")
	else:
		player.color("white")
		

	if enemy.xcor() > 280 or enemy.xcor() < -280: 
		enemyspeed *= -1
        	enemy.sety(enemy.ycor() - 20 )
	
	bullet.sety(bullet.ycor() + bulletspeed)	
	
	if bullet.ycor() > 278:	
		bullet.hideturtle()
		bulletstate = True
		bullet.sety(-280)

	if bullet.ycor() < enemy.ycor() + 20 and bullet.ycor() > enemy.ycor() -20 and bullet.xcor() < enemy.xcor() + 20 and bullet.xcor() > enemy.xcor() -20 and bulletstate == False :
		enemy.hideturtle()
		score = addscore(10, score)
		enemy.setposition(-200,250)
		enemy.showturtle()
		
		logo.clear()
		drawlogo("Space Invader: " + str(score))




 		
wn.bye()
#delay = raw_input("Thanks For Playing.  You got " + str(score) +" Points Press enter to finish")

