#!/bin/python
#imports the modules used

#Needs the Open sans font to be installed
import turtle
import os.path
import sys
#import time
import random
import json
#sets up the screen 
wn = turtle.Screen()
#Prevents errors on close 
def on_close():
	global gameloop
	gameloop = False

wn.getcanvas().winfo_toplevel().protocol("WM_DELETE_WINDOW", on_close)


wn.bgcolor("black")
wn.title("Space Invaders")


#Checks if X is an valid int
def isint(check):
	try:
		int(check)
		return True
	except ValueError:
		return False

#Settings file
#json parser
def jsonP(jfile):
	if os.path.isfile(jfile):
		try:
			with open(jfile,"r") as f:
				return json.load(f)
		except ValueError:
			print("Faild to decode")
			return True	
	else:
		return False
#Checks if -l was used
if len(sys.argv) > 2:
	#print("test")
	if sys.argv[-2] == "-l":
		custom = jsonP(sys.argv[-1]+".json")
		if custom == False:
			print(sys.argv[-1]+".json","was not found")
			espeed = 2
			health = 5
			image = "ship.gif"
			ensp = "enemy2.gif"
			pbullet = "triangle"
			amount_of_enemies = 7
		elif custom != True:
			if "speed" in custom and isint(custom["speed"]):
				espeed = custom["speed"]			
			else:
				espeed = 2
			if "health" in custom and isint(custom["health"] and custom["health"] > 0):
				health = custom["health"]
			else:
				health = 5
			if "player" in custom and os.path.isfile(custom["player"]):
				image = custom["player"]
			else:
				image = "ship.gif"
			if "enemy" in custom and os.path.isfile(custom["enemy"]):
				ensp = custom["enemy"]
			else:
			
				ensp = "enemy2.gif"
			if "bullet" in custom and os.path.isfile(custom["bullet"]):
				pbullet = custom["bullet"]
				wn.register_shape(pbullet)
			else:
				pbullet = "triangle"
			if "bg" in custom and os.path.isfile(custom["bg"]):
				wn.bgpic(custom["bg"]) 
			if "enemies" in custom and isint(custom["enemies"]) and custom["enemies"] > 0 and custom["enemies"] <= 11:

				amount_of_enemies = custom["enemies"]
			else:
				amount_of_enemies = 7
			if "deathline" in custom and isint(custom["deathline"] and custom["deathline"] > -290 and custom["deathline"] < 280 ):
				dl = custom["deathline"]
			else:
				dl = -280	
		else:
			espeed = 2          	
			health = 5
			image = "ship.gif"
			ensp = "enemy2.gif"
			pbullet = "triangle"
			amount_of_enemies = 7
			dl = -280
else:
	espeed = 2
	health = 5
	image = "ship.gif"
	ensp = "enemy2.gif"
	pbullet = "triangle"
	amount_of_enemies = 7		
#Testing zone!!
#The sprites will only be loaded if they are present  









if os.path.isfile(ensp):
	wn.register_shape(ensp)
else:
	ensp = "circle"
if os.path.isfile(image):
	wn.register_shape(image)
else: 
	image = "triangle"
wn.update()
#Writes the log with the scores. Used for high score. Can be used to write anything to the log file.
def writelog(text): 
	log = "score.txt" 
	if os.path.isfile(log): 
		#print("file exists") 
		logt = open( log,"a") 
	else: 
		logt = open(log, "w") 
	logt.write(text + "\n")
#Sets the content of the file to ""
def clearLog(path):
	if os.path.isfile(path):
		log = open(path,"w")
		log.write("")
	else:
		print("file not found")
#Reads the score file
def readlog(name):
	if os.path.isfile(name):
		log = open(name, "r")
		cont = log.read()
		return cont	
	else:
		print("File",name,"not found")
		return False


#Prints an array
def printList(List):
	for i in List:
		print(i)

#Sorts the high score 
def sortHighScore(scores):
	scores = scores.splitlines()
	temp = []		
	for x in scores:
		if isint(x):
			temp.append(int(x))			
	if len(temp) > 0:
		temp.sort(reverse = True)	
		return temp
	else:
		return False

#Checks for startup arguments 
if len(sys.argv) > 1:
	if sys.argv[1] in "Score":
		log = readlog("score.txt")
		if log != False:
			log = sortHighScore(log)	
			if log != False:
				print("High scores")
				printList(log)
			else:
				print("The Score file is invalid or empty")
		else:
			print("No high scores found")
	elif sys.argv[1] in "clear": #Checks for startup arguments 

		clearLog("score.txt")
#penup = hides traces 



#Text class. Creates a class used for writing text to the screen. Uses turtule.Turtle as it's super class

class Text(turtle.Turtle):
	def __init__(self,color, x,y,text):
		turtle.Turtle.__init__(self)
		self.color(color)
		self.penup()
		self.hideturtle()
		self.setposition(x,y)
		#self.speed(0)
		self.write(text, move=False, align='center', font=('Open Sans', 12, 'normal'))

#Redrawing function. Used for updating the text
	def redraw(self,text):

		self.write(text, move=False, align='center', font=('Open Sans', 12, 'normal'))
#Creates the logo
logo = Text("white",0,310,"Space Invader")

class deathline(turtle.Turtle):
	def __init__(self,color,x,y,text):
		turtle.Turtle.__init__(self)
		self.color(color)
		self.penup()
		self.hideturtle()
		self.setposition(x,y)
		self.pendown()
		self.fd(-600)
		#self.speed(0)
		self.penup()
		#self.setposition(0,x)
	

line = deathline("red",300,200,"ff")
#Draw border 
def drawborder(color):

	border_pen = turtle.Turtle()
	border_pen.hideturtle()
	border_pen.speed(10)
	border_pen.color(color)
	border_pen.penup()
	border_pen.setposition(-300,-300) 
	border_pen.pensize(3)
	border_pen.pendown()
#Draws for sides
	for side in range(4):
		border_pen.fd(600)
		border_pen.lt(90)




def setEpos(enemies,i):
	tmp = []
	for x in range(len(enemies)):
		if i != x:
			if enemies[x].ycor() > 240:
				tmp.append(enemies[x].xcor())
	enemies[i].espeed = espeed	
	if len(tmp) > 0:
	
		tmp.sort()
		#print(tmp)	
		if tmp[-1] < 240:
			#print("ytest")
			
			enemies[i].sety(250)
			enemies[i].setx(tmp[-1] + 50)
		elif tmp[0] > -240:
			enemies[i].sety(250)
			enemies[i].setx(tmp[0]-50)
	else:
		enemies[i].sety(250)
		enemies[i].setx(0)
		#else:
		#To be fixed	
			#for x in tmp:
			#	print("lol")
					
#Player class. Uses turtle.Turtle as the super class
class user(turtle.Turtle):
	def __init__(self,color, shape,x,y,heading, speed, health):
		turtle.Turtle.__init__(self)
		self.color(color)
		self.shape(shape)
		self.penup()
		self.speed(0)
		self.setposition(x,y)
		self.setheading(heading)
		#player probs
		self.playerspeed = speed
		self.health = health


#Creates the player by copying the user class 
player = user("red", image,0,-250,0,20, health)

#Sets up the gui
health = Text("white",-260,310,("Health: " + str(player.health)))
#Old gui elements. Unsed as time trail was never implemented
#time = 20
#timeText = Text("white", 250,310,"Time: ")
#timeT = Text("white", 290,309,time)

#Player weapon
class proj(turtle.Turtle):
	def __init__(self,color,rot, shape, speed,state):
		turtle.Turtle.__init__(self)
		self.color(color)
		self.shape(shape)
		self.penup()
		self.speed(0)
		self.setheading(rot)
		self.shapesize()
		self.hideturtle()
		self.bulletspeed = speed
		self.bulletstate = state
		self.setx(0)
		self.sety(0)
bullet = proj("red",90,pbullet, 30,True )
ebullet = proj("blue",-90,"triangle", 30,False )



#Game varibales
gameloop = True
score = 0
#Ends the game. gameloop must be global to save on cpu power
def endgame():
	global gameloop
	gameloop = False

#Score add func 
def addscore(ascore, score):
 
	score+=ascore
	return score

	#move the player left
def move_left():
	x = player.xcor()-player.playerspeed
	if x < -280:
		x = -280
	player.setx(x)


#move right
def move_right():
	x = player.xcor()+player.playerspeed
	if x > 280:
		x = 280
	player.setx(x)

#bulletsate is global to save on cpu power and to make the key lisitner work without a class 
def fire_bullet(): 
	#Declare bullet state as global
         
        
	#moves the bullet 
	if bullet.bulletstate:
		x = player.xcor()
		y = player.ycor() + 20
		bullet.setposition(x, y)
		bullet.showturtle()
		bullet.bulletstate = False

def fire_ebullet(i):
	x = enemies[i].xcor()
	y = enemies[i].ycor() -20
	ebullet.setposition(x,y)
	ebullet.showturtle()



#enemy 
#Enemy class. uses the turtle.Turtle class as it super class 
class enemyi(turtle.Turtle):
	def __init__(self,x,y):	
		turtle.Turtle.__init__(self)
		self.espeed = espeed
		self.color("red")
		self.shape(ensp)
		self.penup()
		self.speed(0)
		self.setposition(x,y)

enemies= []
epos = [-250,250]
for i in range(amount_of_enemies):	
	enemies.append(enemyi(epos[0],epos[1]))
	epos[0]+= 50



#setEpos(enemies,3)

#create keybaord listiner
turtle.listen()
turtle.onkey(move_left,"Left")
turtle.onkey(move_right, "Right")
turtle.onkey(fire_bullet, "space")
#Ends the game when q is pressed
turtle.onkey(endgame, "q")



#Draws the border 
drawborder("white")
#Keeps track of the score
score
ebullet.bulletstatfe = False
btr = False
#Makes the code only check hit detecstion every 2nd cycle insted of every cycle
tick = 0
#Gameloop. Uses the global gameloop bool
fire = 0
while gameloop:
	#Makes the enemies move 	
	for i in range(len(enemies)):
		enemies[i].setx(enemies[i].xcor()- enemies[i].espeed)
		#Makes the enemies move down if they are at the edge of the "screen"
		if enemies[i].xcor() > 280 or enemies[i].xcor() < -280: 
			enemies[i].espeed *= -1
			enemies[i].sety(enemies[i].ycor() - 30 )
			if enemies[i].ycor() < - 270:
				gameloop = False 


		if bullet.bulletstate == False:
		#Makes the code only check hits every other cpu cycle to save on cpu
			if tick < 2:
			#print("Pass")	
			#Pre setting the variabls to save on cpu
				
				enemyy = enemies[i].ycor()
				#Compares the posistion of the enmies compared to the bullet
				if bullet.ycor() < enemyy + 40 and bullet.ycor() > enemyy -40:
					enemyx = enemies[i].xcor() 	
					if bullet.xcor() < enemyx + 40 and bullet.xcor() > enemyx -40 and bullet.bulletstate == False:
						#Hides the enemy
						enemies[i].hideturtle()
						#Adds 10 points to the score
						score = addscore(10, score)
						#Restes the postion of the enemy 
						#enemies[i].setposition(-200,250)
						setEpos(enemies,i)
						#Shows the enmie after its postion has been rested
						enemies[i].showturtle()
						#Hides the bullet
						bullet.hideturtle()		
						#Makes the bullet ready to be fired again also prevents it from hitting more then one enemy
						bullet.bulletstate = True
						logo.clear()
						logo.redraw("Space Invader: " + str(score))


	
	
	
	#Makes the code only move the bullet when it fired	
	if bullet.bulletstate == False:	
		bullet.sety(bullet.ycor() + bullet.bulletspeed)	
		#Resets the bullet when it reatched the end of the screen	
		if bullet.ycor() > 278:	
			bullet.hideturtle()
			bullet.bulletstate = True
			bullet.sety(-280)

	if fire < 2 and ebullet.bulletstate == False:
		#print("pass")
		enb = random.randrange(0,len(enemies))
		ebullet.bulletstate = True
		fire_ebullet(enb)
	elif ebullet.bulletstate == True:
		
		ebullet.sety(ebullet.ycor() - ebullet.bulletspeed)
		#Only checks enemy bullets if the it as the bottom of the screen	
		if ebullet.ycor() < -270:
		
			if ebullet.xcor() + 20 > player.xcor() and ebullet.xcor() -20 < player.xcor():
				player.health = player.health - 1
				health.clear()
				health.redraw("Health: "+ str(player.health))
				 		           	
			ebullet.hideturtle()
			ebullet.bulletstate = False
        			#ebullet.sety(-280)
				
	if player.health < 1:
		gameloop = False
	#Prevents the code from checking hit detecstion when the bullet is not fired 


#Makes the tick work 
	
	#logo.clear()
	#print(str(tick))
#resets tick to to 0 every other cycle 
	fire+= 1
	#print(fire) 
	if fire > 30:
		fire = 0
	if tick == 2:
		tick = 0
	tick +=1
#Writes the score file if the score is above 0
if score > 0:
	writelog(str(score))



	
wn.bye()





print("Thanks For Playing.  You got " + str(score) +" Points")

