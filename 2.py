#!/bin/python2.7
#imports the modules used
#Must be run with python 2.7
#Needs the Open sans font to be insalled
#Made in January 2018. Finished in September
import turtle
import os.path
import sys
import time
import random
import json
#sets up the screen 
wn = turtle.Screen()
wn.bgcolor("black")
wn.title("Space Invaders")

#Settings file


#json parser

#Testing zone!!
#The sprites will only be loaded if they are present  
image = "ship.gif"
ensp = "enemy2.gif"
if os.path.isfile(ensp):
	wn.register_shape(ensp)
else:
	ensp = "circle"
if os.path.isfile(image):
	wn.register_shape(image)
else: 
	image = "triangle"

#Writes the log with the scores. Used for high score. Can be used to write anything to the log file.
def writelog(text): 
	log = "score.txt" 
	if os.path.isfile(log): 
		#print("file exists") 
		logt = open( log,"a") 
	else: 
		logt = open(log, "w") 
	logt.write(text + "\n")
#Sets the content of the file to ""
def clearLog(path):
	if os.path.isfile(path):
		log = open(path,"w")
		log.write("")
	else:
		print("file not found")
#Reads the score file
def readlog(name):
	if os.path.isfile(name):
		log = open(name, "r")
		cont = log.read()
		return cont	
	else:
		print("File",name,"not found")
		return False
#Checks if it is an int 
def isint(check):
	try:
		int(check)
		return True
	except ValueError:
		return False
	
#Prints an array
def printList(List):
	for i in List:
		print(i)

#Sorts the high score 
def sortHighScore(scores):
	scores = scores.splitlines()
	temp = []		
	for x in scores:
		if isint(x):
			temp.append(int(x))			
	if len(temp) > 0:
		temp.sort(reverse = True)	
		return temp
	else:
		return False

#Checks for startup arguments 
if len(sys.argv) > 1:
	if sys.argv[1] in "Score":
		log = readlog("score.txt")
		if log != False:
			log = sortHighScore(log)	
			if log != False:
				print("High scores")
				printList(log)
			else:
				print("The Score file is invalid or empty")
		else:
			print("No high scores found")
	elif sys.argv[1] in "clear":
		clearLog("score.txt")
#penup = hides traces 









#Text class. Creates a class used for writing text to the screen. Uses turtule.Turtle as it's super class

class Text(turtle.Turtle):
	def __init__(self,color, x,y,text):
		turtle.Turtle.__init__(self)
		self.color(color)
		self.penup()
		self.hideturtle()
		self.setposition(x,y)
		#self.speed(0)
		self.write(text, move=False, align='center', font=('Open Sans', 12, 'normal'))

#Redrawing function. Used for updating the text
	def redraw(self,text):

		self.write(text, move=False, align='center', font=('Open Sans', 12, 'normal'))
#Creates the logo
logo = Text("white",0,310,"Space Invader")





#Draw border 
def drawborder(color):

	border_pen = turtle.Turtle()
	border_pen.hideturtle()
	border_pen.speed(10)
	border_pen.color(color)
	border_pen.penup()
	border_pen.setposition(-300,-300) 
	border_pen.pensize(3)
	border_pen.pendown()
#Draws for sides
	for side in range(4):
		border_pen.fd(600)
		border_pen.lt(90)




#Player class. Uses turtle.Turtle as the super class
class user(turtle.Turtle):
	def __init__(self,color, shape,x,y,heading, speed, health):
		turtle.Turtle.__init__(self)
		self.color(color)
		self.shape(shape)
		self.penup()
		self.speed(0)
		self.setposition(x,y)
		self.setheading(heading)
		#player probs
		self.playerspeed = speed
		self.health = health


#Creates the player by copying the user class 
player = user("red", image,0,-250,0,20, 5)

#Sets up the gui
health = Text("white",-260,310,("Health: " + str(player.health)))
#Old gui elements. Unsed as time trail was never implemented
#time = 20
#timeText = Text("white", 250,310,"Time: ")
#timeT = Text("white", 290,309,time)

#Player weapon
class proj(turtle.Turtle):
	def __init__(self,color,rot, shape, speed):
		turtle.Turtle.__init__(self)
		self.color(color)
		self.shape(shape)
		self.penup()
		self.speed(0)
		self.setheading(rot)
		self.shapesize()
		self.hideturtle()
		self.bulletspeed = speed
		self.bulletstate = True	

bullet = proj("red",90,"triangle", 30 )
ebullet = proj("blue",-90,"triangle", 30 )

#enemyb.showturtle()

#bulletspeed = 30
#Define bullet state
#bulletstate = True

#Game varibales
gameloop = True
score = 0
#Ends the game. gameloop must be global to save on cpu power
def endgame():
	global gameloop
	gameloop = False

#Score add func 
def addscore(ascore, score):
 
	score+=ascore
	return score

	#move the player left
def move_left():
	x = player.xcor()-player.playerspeed
	if x < -280:
		x = -280
	player.setx(x)


#move right
def move_right():
	x = player.xcor()+player.playerspeed
	if x > 280:
		x = 280
	player.setx(x)

#bulletsate is global to save on cpu power and to make the key lisitner work without a class 
def fire_bullet(): 
	#Declare bullet state as global
         
        
	#move the bullet 
	if bullet.bulletstate:
		x = player.xcor()
		y = player.ycor() + 20
		bullet.setposition(x, y)
		bullet.showturtle()
		bullet.bulletstate = False

def fire_ebullet(i):
	x = enemies[i].xcor()
	y = enemies[i].ycor() -20
	ebullet.setposition(x,y)
	ebullet.showturtle()
	

#enemy 
#Enemy class. uses the turtle.Turtle class as it super class 
class enemyi(turtle.Turtle):
	def __init__(self,x,y):	
		turtle.Turtle.__init__(self)
		self.espeed = 2
		self.color("red")
		self.shape(ensp)
		self.penup()
		self.speed(0)
		self.setposition(x,y)

#Declar enemies  x and y starting postions 
enemy = enemyi(-250,250)
enemy2 = enemyi(-200,250)
enemy3 = enemyi(-150,250)
enemy4 = enemyi(-100,250)
enemy5 = enemyi(-50,250)
enemy6 = enemyi(0,250)
#To add more enemies declar one like the ones above and then add it to the array
#Creates and array with all of the enemies
enemies = [enemy,enemy2, enemy3, enemy4, enemy5, enemy6]


#create keybaord listiner
turtle.listen()
turtle.onkey(move_left,"Left")
turtle.onkey(move_right, "Right")
turtle.onkey(fire_bullet, "space")
#Ends the game when q is pressed
turtle.onkey(endgame, "q")



#Draws the border 
drawborder("white")
#Keeps track of the score
score
ebullet.bulletstate = False
btr = False
#Makes the code only check hit detecstion every 2nd cycle insted of every cycle
tick = 0
#Gameloop. Uses the global gameloop bool
fire = 0
while gameloop:
	#Makes the enemies move 	
	for i in range(len(enemies)):
		enemies[i].setx(enemies[i].xcor()- enemies[i].espeed)
	if player.xcor() == 280 or player.xcor() == -280:
		player.color("red")
		
		
	else:
		player.color("white")
	#Makes the enemies move down if they are at the edge of the "screen"
	for i in range(len(enemies)):
		 
		if enemies[i].xcor() > 280 or enemies[i].xcor() < -280: 
			enemies[i].espeed *= -1
			enemies[i].sety(enemies[i].ycor() - 30 )
			if enemies[i].ycor() < - 270:
				gameloop = False 
	#Makes the code only move the bullet when it fired	
	if bullet.bulletstate == False:	
		bullet.sety(bullet.ycor() + bullet.bulletspeed)	
		#Resets the bullet when it reatched the end of the screen	
		if bullet.ycor() > 278:	
			bullet.hideturtle()
			bullet.bulletstate = True
			bullet.sety(-280)

	if fire < 2 and ebullet.bulletstate == False:
		#print("pass")
		enb = random.randrange(0,len(enemies))
		ebullet.bulletstate = True
		btr = True
	elif ebullet.bulletstate == True:
		#print("Real pass")
		if btr == True:
			fire_ebullet(enb)
			btr = False
		else:
			ebullet.sety(ebullet.ycor() - bullet.bulletspeed)
			

			if ebullet.ycor() < -270 :
				if ebullet.xcor() + 20 > player.xcor() and ebullet.xcor() -20 < player.xcor():
					player.health = player.health - 1
					health.clear()
					health.redraw("Health: "+ str(player.health))
						           	
				ebullet.hideturtle()
				ebullet.bulletstate = False
        			#ebullet.sety(-280)
				btr = True
	if player.health < 1:
		gameloop = False
	#Prevents the code from checking hit detecstion when the bullet is not fired 
	if bullet.bulletstate == False:
		#Makes the code only check hits every other cpu cycle to save on cpu
		if tick < 2:
			#print("Pass")	
			for i in range(len(enemies)):
				#Pre setting the variabls to save on cpu
				
				enemyy = enemies[i].ycor()
				#Compares the posistion of the enmies compared to the bullet
				if bullet.ycor() < enemyy + 40 and bullet.ycor() > enemyy -40:
					enemyx = enemies[i].xcor() 	
					if bullet.xcor() < enemyx + 40 and bullet.xcor() > enemyx -40 and bullet.bulletstate == False:
						#Hides the enemy
						enemies[i].hideturtle()
						#Adds 10 points to the score
						score = addscore(10, score)
						#Restes the postion of the enemy 
						enemies[i].setposition(-200,250)
						#Shows the enmie after its postion has been rested
						enemies[i].showturtle()
						#Hides the bullet
						bullet.hideturtle()		
						#Makes the bullet ready to be fired again also prevents it from hitting more then one enemy
						bullet.bulletstate = True
						logo.clear()
						logo.redraw("Space Invader: " + str(score))


#Makes the tick work 
	
	#logo.clear()
	#print(str(tick))
#resets tick to to 0 every other cycle 
	fire+= 1
	#print(fire) 
	if fire > 30:
		fire = 0
	if tick == 2:
		tick = 0
	tick +=1
if score > 0:
	writelog(str(score))
	
wn.bye()
print("Thanks For Playing.  You got " + str(score) +" Points")

